import eyeD3 as id3
import sqlite3
import os, re, sys
import vlc
from googlevoice import Voice
import BeautifulSoup
import time
import json
import httplib
from selenium import webdriver

from threading import Timer
import re

REMOTEONLY = False
DATABASE = '/Users/willskinner/.musicdb.db'
GROOVESHARKAPIKEY = 'a98577268f39006784f0601e6962e821'
if REMOTEONLY:
    DATABASE = '/Users/willskinner/.musicdb2.db'

def initialize(path):
    dbCreateTable()
    dbCreateIndex()
    dbPopulateTable(path)
    dbPopulateIndex()

# Create a new table at location DATABASE to hold music 
def dbCreateTable():
    musicdb = DATABASE
    connection = sqlite3.connect(musicdb)
    cursor = connection.cursor()
    cursor.execute('CREATE TABLE musictable (Id INTEGER PRIMARY KEY, Artist TEXT, Title TEXT, Album TEXT, Date TEXT, \
            Year TEXT, Genre TEXT, TrackNum TEXT, DiscNum TEXT, Comments TEXT, Lyrics TEXT, Location TEXT, Hash TEXT, UNIQUE(Hash))')
    connection.commit()
    cursor.close()



# Create a new FTS database at location INDEX
def dbCreateIndex():
    musicfts = DATABASE
    connection = sqlite3.connect(musicfts)
    cursor = connection.cursor()
    cursor.execute('CREATE VIRTUAL TABLE musicfts USING fts4(artist, title, album, genre, id)')
    connection.commit()
    cursor.close()

# Populate the song database
def dbPopulateTable(path):
    connection = sqlite3.connect(DATABASE)
    cursor = connection.cursor()
    count = dbAddFolder2(path, connection, cursor)
    connection.commit()
    connection.close()
    print 'Added %s songs out of %s' % count

# Build the search index
def dbPopulateIndex():
    connection = sqlite3.connect(DATABASE)
    cursor = connection.cursor()
    results = cursor.execute('SELECT * FROM musictable').fetchall()
    connection.commit()
    cursor.close()
    connection = sqlite3.connect(DATABASE)
    cursor = connection.cursor()
    count = 0
    for song in results: 
        data = (song[12], song[1], song[2], song[3], song[6], song[12])
        print data
        try:
            cursor.execute('INSERT INTO musicfts(docid, artist, title, album, genre, id) VALUES(?, ?, ?, ?, ?, ?)', data)
            count += 1
        except sqlite3.IntegrityError:
            print 'Duplicate element'
        connection.commit()
    print 'Indexed ' + str(count) + ' songs'
    print str(len(results)) + ' songs in the database'
    connection.close()

# Recursively add the mp3 files from a folder to the database
def dbAddFolder(path, connection, cursor):
    if path.split('/').pop() == 'iTunes':
        return 0, 0
    songcount = 0
    totalCount = 0
    for element in os.listdir(path):
        totalCount += 1
        element = element
        ext = element.split('.').pop();
        if ext == 'mp3':
            try:
                dbAddSong(path + '/' + element, connection, cursor)
                songcount += 1
            except:
                print 'There was an exception when adding the song: %s ' % path
        elif ext == element:
            try:
                print 'adding the files from %s' % path + '/' + element
                songcount += dbAddFolder(path + '/' + element, connection, cursor)
            except:
                continue
        else:
            continue
    return songcount, totalCount

# Use os.walk
def dbAddFolder2(path, connection, cursor):
    count = 0
    totalCount = 0
    extensions = ['mp3', 'mp4', 'ogg', 'wav', 'aac', 'm4p', 'flac']
    for i in os.walk(path):
        for f in i[2]:
            if f.split('.').pop() in extensions:
                totalCount += 1
                try:
                    dbAddSong(i[0] + '/' + f, connection, cursor)
                    count += 1
                except:
                    print 'There was an exception'
    return count, totalCount

def strConvert(x):
    if isinstance(x, basestring):
        if x == u'':
            # print str(x) + ' was empty'
            return None
        if isinstance(x, unicode):
            # print x + ' was unicode'
            return unicode(x.encode('utf8', errors = 'ignore'), errors = 'ignore')
        # print str(x) + ' was not empty or unicode'
        return unicode(x, errors = 'ignore')
    else:
        # print str(x) + ' was not already a string'
        return str(x)

def dbAddSong(path, connection, cursor):
    tag = id3.Tag()
    tag.link(path)
    artist = tag.getArtist()
    album = tag.getAlbum()
    title = tag.getTitle()
    genre = str(tag.getGenre())
    bytePath = unicode(path, errors = 'ignore')
    h = abs(hash(artist + album + title + bytePath))
    data = [strConvert(x) for x in (artist, title, album, tag.getDate(), tag.getYear(), genre, tag.getTrackNum(), tag.getDiscNum(), tag.getComments(), tag.getLyrics(), path, h)]
    os.rename(path, strConvert(path))
    print 'adding song ' + path
    print data
    cursor.execute('INSERT OR IGNORE INTO musictable VALUES(null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', data)
    connection.commit()

# Return the list of songs matching the query
def dbQuery(query):
    pass   

# Return k randomly selected elements from the list
def randK(ls, k):
    if k == 0:
        return []
    else:
        term = random.choice(ls)
        ls.remove(term)
        return randK(ls, k-1).append(term)

def start(cred0, cred1):
    p = PlayerServer(cred0, cred1)
    print 'Established connection to Google Voice'
    p.listen()
    print 'Server ready'


class PlayerServer:
    """
    Encapsulates functionality required by a running NotQ server.
    """

    def __init__(self, email, password):
        self.voice = Voice()
        self.voice.login(email, password)
        # A dictionary of lists, with one entry corresponding to each phone number from which messages have been received.
        self.messages = {}
        self.actionQueue = []
        # A dictionary of numbers to lists. Each list contains the full history of the Actions performed on behalf of the number
        self.actionHistory = {}
        self.vlc = vlc.Instance()
        # A queue of tuples (phoneNumber, songHash) 
        self.songPlayingNow = False

        self.mediaPlayer = self.vlc.media_player_new()

        self.songQueue = []

        self.currentSongIndex = 0

        self.events = self.mediaPlayer.event_manager()
        
        self.browser = webdriver.Firefox()

        self.nowPlaying = False

    def addRemoteMedia(self, phoneNumber = None, mediaType = None, mediaUrl = None):
        """
        Add a non-local media item to the queue.
        """
        mediaData = {'phoneNumber': phoneNumber, 'mediaType': mediatype, 'mediaUrl': mediaUrl}
        self.remoteMediaList.append(mediaData)

    def dumpHistory(self):
        """
        Serialize and store self.actionHistory in the database.
        """
        pass

    def retrieveHistory(self):
        """
        Retrieve and deserialze the self.actionHistory from the database.
        """
        pass

    def mergeMessages(self, currentMessages, voice):
        """
        Merge the messages in the inbox with currentMessages, then return
        the augmented message set. currentMessages is a dictionary of lists of message objects.
        """
        messageSet = set()
        for num, msgs in currentMessages.items():
            for msg in msgs:
                messageSet.add(msg.id)
        remoteMessages = voice.sms().messages
        for msg in remoteMessages:
            if msg.id not in messageSet:
                messageSet.add(msg.id)
                if msg['phoneNumber'] not in currentMessages:
                    currentMessages[msg['phoneNumber']] = []
                currentMessages[msg['phoneNumber']].append(msg)
        return currentMessages
    
    # Merge self.messages with the remote inbox, then delete from both local and remote all messages with isRead == True
    def syncMessages(self):
        self.messages = self.mergeMessages(self.messages, self.voice)
        self.deleteReadMessages(self.messages)
        print 'Sync done'

    def deleteReadMessages(self, currentMessages):
        """
        Delete from the local and remote repositories all messages marked isRead.
        """
        for num, msgs in currentMessages.items():
            for msg in msgs:
                if msg.isRead:
                    msg.delete()
                    msgs.remove(msg)

    def songDoneCallback(self, *args, **kwargs):
        print 'The song ended'
        if len(self.songQueue) > 0:
            nextSong = self.songQueue.pop(0)
            self.play(nextSong[0], nextSong[1])
        else:
            self.songPlayingNow = False
            print 'There are no more songs in the queue!'

    def enqueueLocalSong(self, phoneNumber, songHash):
        """
        Add the song to the queue of songs. 
        """
        connection = sqlite3.connect(DATABASE)
        cursor = connection.cursor()
        cursor.execute('select location from musictable where hash = %s' % str(songHash))
        location = cursor.fetchall()[0][0]
        songDict = {'songType': 'local', 'phoneNumber': phoneNumber, 'location': location}
        self.songQueue.append(songDict)

    def enqueueRemoteSong(self, phoneNumber, songUrl):
        songDict = {'songType': 'remote', 'phoneNumber': phoneNumber, 'location': songUrl}
        self.songQueue.append(songDict)

    def remoteDoneCallback(self):
        """
        Intended to be called as soon as a remote song begins.
        """
        self.remotePlayTimer.cancel()
        self.songQueue.pop(0)
        self.play()

    def localDoneCallback(self, *args, **kwargs):
        """
        Same as remoteDoneCallback, but for local songs.
        """
        self.songQueue.pop(0)
        self.play()

    def play(self, paused = False, *args, **kwargs):
        if paused:
            if self.nowPlaying['songType'] == 'local':
                self.mediaPlayer.pause()
            else:
                self.browserPlayPause.click()
                self.browserTimingCallback()
        else:
            if len(self.songQueue) > 0:
                nextSong = self.songQueue[0]
                if nextSong['songType'] == 'local':
                    self.mediaPlayer = vlc.MediaPlayer(nextSong['location'])
                    self.handleVolume(u'+15108470608', 75)
                    self.nowPlaying = nextSong
                    self.events = self.mediaPlayer.event_manager()
                    self.events.event_attach(vlc.EventType.MediaPlayerEndReached, self.localDoneCallback, 1)
                    self.mediaPlayer.play()
                elif nextSong['songType'] == 'remote':
                    self.mediaPlayer = vlc.MediaPlayer()
                    self.remotePlayer = self.playGrooveshark(nextSong['location'])
                    self.nowPlaying = nextSong
                else:
                    print 'The song type was not correctly specified'

    def pause(self, phoneNumber):
        if phoneNumber == u'+15108470608':
            if self.mediaPlayer.is_playing():
                self.mediaPlayer.pause()
            else:
                self.browserPlayPause.click()
                self.remotePlayTimer.cancel()

    def handleVolume(self, phoneNumber, vol):
        if phoneNumber == u'+15108470608':
            vlc.libvlc_audio_set_volume(self.mediaPlayer, vol)

    def handleVolumeUp(self, phoneNumber):
        if phoneNumber == u'+15108470608':
            currentVol = vlc.libvlc_audio_get_volume(self.mediaPlayer)
            self.handleVolume(phoneNumber, currentVol + 5)

    def handleVolumeDown(self, phoneNumber):
        if phoneNumber == u'+15108470608':
            currentVol = vlc.libvlc_audio_get_volume(self.mediaPlayer)
            self.handleVolume(phoneNumber, currentVol - 5)

    def search(self, query):
        pass

    def listen(self):
        while True:
            self.syncMessages()
            print self.messages.items()
            flatMessages = [item for number in self.messages.values() for item in number]
            print 'Got %s messages' % len(flatMessages)
            for number, msgs in self.messages.items():
                print msgs
                for msg in msgs:
                    self.handleMessage(number, msg)
                    msg.isRead = True
            time.sleep(5)

    def act(self):
        while True:
            while not self.actionQueue is []:
                msg = self.actionQueue.pop()
                self.handleMessage(msg)

    def handleMessage(self, number, msg):
        tokenizedText = msg['messageText'].lower().split(' ')
        keyword = tokenizedText[0].lower()
        phoneNumber = msg.phoneNumber
        # should accept a song title hash as argument
        if keyword == 'play':
            print 'Playing'
            if len(tokenizedText) == 1:
                self.play(paused = True)
            else:
                self.handlePlay(phoneNumber, msg, tokenizedText)
        elif keyword == 'pause' or keyword == 'paws':
            print 'Pausing'
            self.handlePause(phoneNumber, msg)
        elif keyword == 'volume':
            print 'Changing the volume'
            token2 = tokenizedText[1]
            if token2 == 'up':
                self.handleVolumeUp(phoneNumber)
            elif token2 == 'down':
                self.handleVolumeDown(phoneNumber)
            else:
                vol = int(token2)
                self.handleVolume(phoneNumber, vol)
        elif keyword == 'search':
            print 'Got a search message'
            self.handleSearch(phoneNumber, msg, tokenizedText)
        elif keyword == 'more':
            print 'Sending more results'
            self.handleMore(phoneNumber, msg)
        else:
            pass

    def handleMore(self, phoneNumber, msg):
        try:
            actions = self.actionHistory[phoneNumber]
        except KeyError:
            print 'You have to do a search before you can get more results'
            self.sendMessage(phoneNumber, 'You have to do a search before you can get more results')
            return
        lastSearch = None
        for action in actions:
            if 'actionType' in action:
                if action['actionType'] == 'localSearch' or action['actionType'] == 'remoteSearch':
                    lastSearch = action
                    break
        if not lastSearch:
            print 'You have to do a search before you can get more results'
            self.sendMessage(phoneNumber, 'You have to do a search before you can get more results')
            return
        startIndex = lastSearch['numResultsSent']
        response = self.getResponseString(lastSearch['searchResults'], startIndex - 1)
        responseString = response[0]
        self.sendMessage(phoneNumber, responseString)
        actionDict = {'actionType': 'search', 'searchResults': lastSearch['searchResults'], 'message': msg,\
                'numResultsSent': response[1] + lastSearch['numResultsSent']}
        self.storeAction(phoneNumber, actionDict)


    def handlePause(self, phoneNumber, msg):
        self.pause(phoneNumber)
        actionDict = {'actionType': 'pause', 'message': msg}
        self.storeAction(phoneNumber, actionDict)

    def handlePlay(self, phoneNumber, msg, tokenizedText):
        """
        """
        songNumber = int(tokenizedText[1])
        try:
            lastAction = None
            for action in self.actionHistory[phoneNumber]:
                if action['actionType'] == 'localSearch' or \
                        action['actionType'] == 'remoteSearch' and action['numResultsSent'] > 0:
                    lastAction = action
                    break
        except KeyError:
            print 'No actionHistory entry for this number. Try searching again first'
            return
        if not lastAction:
            print 'No actionHistory entry for this number. Try searching again first'
        if lastAction['actionType'] == 'localSearch':
            songHash = lastAction['searchResults'][songNumber][4]
            self.enqueueLocalSong(phoneNumber, songHash)
            actionDict = {'actionType': 'play', 'message': msg, 'songHash': songHash}
        else:
            url = lastAction['searchResults'][songNumber]['Url']
            self.enqueueRemoteSong(phoneNumber, url)
            actionDict = {'actionType': 'play', 'message': msg, 'url': url}
        if len(self.songQueue) == 1:
            self.play()
        self.storeAction(phoneNumber, actionDict)

    def handleSearch(self, phoneNumber, msg, tokenizedText):
        self.clearMessageHistory(phoneNumber)
        columns = ['artist', 'title', 'album', 'date', 'year', 'genre', 'tracknum', 'discnum', 'comments', \
                'lyrics', 'location', 'hash']
        token2 = tokenizedText[1].lower()
        connection = sqlite3.connect(DATABASE)
        cursor = connection.cursor()
        if token2 in columns:
            query = 'select * from musicfts where %s match "%s"' % (token2, ' '.join(tokenizedText[2:]).lower())
        else:
            query = 'select * from musicfts where musicfts match "%s"' % ' '.join(tokenizedText[1:]).lower()
        cursor.execute(query)
        results = cursor.fetchall()
        response = self.getResponseString(results)
        responseString = response[0]
        if responseString == '':
            remoteResults = self.searchGrooveshark(' '.join(tokenizedText[1:]))
            response = self.getRemoteResponseString(remoteResults)
            responseString = response[0]
            if responseString == '':
                responseString = 'Your query returned no results. Try a different search.'
                actionDict = {'actionType': 'remoteSearch', 'searchResults': remoteResults, \
                        'message': msg, 'numResultsSent':response[1]}
            else:
                actionDict = {'actionType': 'remoteSearch', 'searchResults': remoteResults, \
                        'message': msg, 'numResultsSent':response[1]}
        else:
            actionDict = {'actionType': 'localSearch', 'searchResults': results, 'message': msg, 'numResultsSent':response[1]}
        self.storeAction(phoneNumber, actionDict)
        self.sendMessage(phoneNumber, strConvert(responseString))

    def handleEnqueue(self, phoneNumber, msg):
        pass

    def storeAction(self, phoneNumber, actionDict):
        try:
            self.actionHistory[phoneNumber].insert(0, actionDict)
            print 'The action was added to actionHistory'
        except KeyError:
            self.actionHistory[phoneNumber] = [actionDict]
            print 'There was no entry for the phone number in actionHistory'

    def getRemoteResponseString(self, queryResults, startIndex = 0):
        """
        Construct a human-readable presentation of the JSON-formatted query results, starting at the given index. 
        """
        responseString = ''
        i = startIndex 
        for song in queryResults:
            title = song['SongName']
            artist = song['ArtistName']
            songListing = '\n' + str(i) + '. ' + title + ' - ' + artist + '\n'
            if len(responseString) + len(songListing) > 420:
                break
            responseString += songListing
            i += 1
        return responseString, i - startIndex

    def getResponseString(self, queryResults, startIndex = 0):
        """
        Construct a human-readable presentation of the query results, starting at the given index
        """
        responseString = ''
        i = startIndex
        for song in queryResults[i:]:
            title = song[1] 
            if title == None:
                title = ''
            artist = song[0]
            if song[0] == None:
                artist = ''
            songListing = '\n' + str(i) + '. ' + title + ' - ' + artist + '\n'
            if len(responseString) + len(songListing) > 420:
                break
            responseString += songListing
            i += 1
        return responseString, i - startIndex


    def sendMessage(self, number, text):
        self.voice.send_sms(number, text)

    def clearMessageHistory(self, phoneNumber):
        """
        Delete the contents of the actionHistory listing for the given number.
        """
        self.actionHistory[phoneNumber] = []
    
    def playGrooveshark(self, url):
        self.browser.get(url)
        self.browserInfoTimer = Timer(10, self.broswerTimingCallback)
        self.browserInfoTimer.start()

    def broswerTimingCallback(self):
        """
        Called a non-blocking way. Sets up the timer which will fire when the current song ends.
        """
        self.browserPlayPause = self.browser.find_element_by_id('player_play_pause')
        pageSoup = BeautifulSoup.BeautifulSoup(self.browser.page_source)
        playerTimes = BeautifulSoup.BeautifulSoup(self.browser.page_source).findAll('span', {'id': 'player_times'})
        elapsedString = pageSoup.findAll('span', {'id': 'player_elapsed'})[0].string.split(':')
        durationString = pageSoup.findAll('span', {'id': 'player_duration'})[0].string.split(':')
        elapsedInt = int(elapsedString[0]) * 60 + int(elapsedString[1])
        durationInt = int(durationString[0]) * 60 + int(durationString[1])
        waitTime = durationInt - elapsedInt
        self.remotePlayTimer = Timer(waitTime, self.remoteDoneCallback)
        self.remotePlayTimer.start()
        self.browserInfoTimer.cancel()

    def stopGrooveshark(self, browser):
        self.browser = None

    def searchGrooveshark(self, query, limit = 32):
        """
        Return the JSON formatted results of querying the Grooveshark database with the given query. 
        """
        cleanQuery = query.strip().replace(' ', '+')
        connection = httplib.HTTPConnection('tinysong.com')
        queryString = '/s/' + cleanQuery + '?format=json&limit=' + str(limit) + '&key=' + GROOVESHARKAPIKEY
        connection.request('GET', queryString) 
        response = connection.getresponse()
        if response.status == 200:
            responseData = json.load(response)
            return responseData
        else:
            print 'Bad response from tinysong.com for query %s: %s' % queryString, response.status
            return None

    def searchGrooveshark(self, query, limit = 10):
        """
        Return the JSON formatted results of querying the Grooveshark database with the given query. 
        """
        cleanQuery = query.strip().replace(' ', '+')
        connection = httplib.HTTPConnection('tinysong.com')
        queryString = '/s/' + cleanQuery + '?format=json&limit=' + str(limit) + '&key=' + GROOVESHARKAPIKEY
        connection.request('GET', queryString) 
        response = connection.getresponse()
        if response.status == 200:
            responseData = json.load(response)
            return responseData
        else:
            print 'Bad response from tinysong.com for query %s: %s' % queryString, response.status
            return None

# Database utilities

def dropTable(path, name):
    connection = sqlite3.connect(path)
    cursor = connection.cursor()
    cursor.execute('DROP TABLE ' + name)
    connection.commit()
    cursor.close()
    print 'Dropped the table'

def printTable():
    connection = sqlite3.connect(DATABASE)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM musictable')
    print cursor.fetchall()

def printSize():
    connection = sqlite3.connect(DATABASE)
    cursor = connection.cursor()
    cursor.execute('SELECT count(*) FROM musictable')
    print cursor.fetchall()


def parseQuery(query):
    keywords = {'random' : randK, 'top' : lambda(ls, k) : ls[:k]}
    terms = query.split(',')
    qDict = {}
    for term in terms:
        filters = term.split(':')
        if len(filters) == 1:
            text = filters[0]
            if text in keywords:
                pass # do some stuff

if __name__ == '__main__':
    start(sys.argv[1], sys.argv[2])
