# To request a song:
1. Text "Search {song, artist, genre, etc}" to 510-423-3897. You will get a numbered list of songs in response.
2. Reply to the text you received with "Play {song number}".
3. There is no step 3.

## Source code: github.com/wskinner/jukebox-hero
