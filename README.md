What is it for?
===============
Jukebox Hero is a jukebox server designed for social occasions like parties. The interface is entirely text message-based so your friends can request tracks individually, whenever they feel like it. The software indexes your local music to make it searchable, just like any other music player.

# Coming Soon
* Intelligent ordering for popular requests
* Web integration so you can request and play songs not in your personal library
* Intelligent request ordering based on user reputations

